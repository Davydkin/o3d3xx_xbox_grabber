import sys
from joystick import Joystick
from camera_interface.camera import CameraInterface
import numpy as np
import os
import datetime
import cv2

DEFAULT_IP = '192.168.10.57'


def joystick_connect():
    """Try and try to connect to the joystick."""
    while True:
        try:
            joy = Joystick()
            break
        except IOError:
            pass
    return joy


def parse_camera_ip():
    """Parse argc and argv to get camera IP."""
    if len(sys.argv) < 2:
        print('Usage: python3 run.py [ip_address]')
        print('Cant find ip address, I will use default one:', DEFAULT_IP)
        return DEFAULT_IP
    # Check control mode
    return sys.argv[1]


def save_data(directory, image, counter):
    np.save(directory + '/' + str(counter), image)
    image = (image > 1000) * 1000 + image
    image = image*255./1000.
    image = image.astype(np.uint8)
    cv2.imwrite(directory + '/' + str(counter) + '.png', image)


def create_new_dir(c):
    print('Creating dir')
    directory = '{0:%Y-%m-%d_%H-%M-%S}'.format(datetime.datetime.now())
    directory = str(c) + '_' + directory + '_data'
    os.mkdir(directory)
    print('Data will be stored in directory:', directory)
    return directory


def main():
    """Run main working loop."""
    print('Trying to connect XBOX controller...')
    print('Press something')
    joy = joystick_connect()
    print('Connected')

    print('Connecting to the camera')
    ip = parse_camera_ip()
    camera = CameraInterface(ip)
    camera.start()
    camera.set_fps(1)
    print('Connected')

    counter = 0
    counter_dir = 0
    is_saving = False
    while True:
        if joy.Start() and not is_saving:
            camera.set_fps(10)
            directory = create_new_dir(counter_dir)
            counter_dir += 1
            is_saving = True
        if joy.Back() and is_saving:
            camera.set_fps(1)
            is_saving = False

        if camera.is_updated():
            image = camera.get()
            if is_saving:
                save_data(directory, image, counter)
                counter += 1
        if camera.is_error():
            print('Error! Something went wrong.')
            break

    joy.close()
    camera.set_fps(1)
    camera.stop()
    return 0


if __name__ == '__main__':
    main()
