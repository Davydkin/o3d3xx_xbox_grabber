"""Module for reading signals from xbox joystick."""
import subprocess
import time
import select
import os


class Joystick:
    """Initializes the joystick/wireless receiver.

    Launch 'xboxdrv' as a
    subprocess and check that the wired joystick or wireless receiver is
    attached. The refreshRate determines the maximnum rate at which events are
    polled from xboxdrv. Calling any of the Joystick methods will cause a
    refresh to occur, if refreshTime has elapsed. Routinely call a Joystick
    method, at least once per second, to avoid overfilling the event buffer.

    Usage:
        joy = xbox.Joystick()
    """

    def __init__(self, refreshRate=30):
        """Init everything."""
        self.proc = subprocess.Popen(
            ['xboxdrv', '--no-uinput', '--detach-kernel-driver'],
            stdout=subprocess.PIPE)
        self.pipe = self.proc.stdout
        # will be set to True once controller is detected and stays on
        self.connectStatus = False
        # initialize stick readings to all zeros
        self.reading = '0' * 140
        # absolute time when next refresh is to occur
        self.refreshTime = 0
        # joystick refresh is to be performed 30 times per sec by default
        self.refreshDelay = 1.0 / refreshRate

        # Read responses from 'xboxdrv' for upto 2 seconds,
        # looking for controller/receiver to respond
        found = False
        waitTime = time.time() + 2
        while waitTime > time.time() and not found:
            readable, writeable, exception = select.select(
                [self.pipe], [], [], 0)
            if readable:
                response = self.pipe.readline()
                # Hard fail if we see this, so force an error
                if response[0:7] == 'No Xbox':
                    raise IOError('No Xbox controller/receiver found')
                # Success if we see the following
                if response[0:12].lower() == 'press ctrl-c':
                    found = True
                # If we see 140 char line, we are seeing valid input
                if len(response) == 140:
                    found = True
                    self.connectStatus = True
                    self.reading = response
        # if the controller wasn't found, then halt
        if not found:
            self.close()
            raise IOError('Unable to detect Xbox controller - use sudo')

    """Used by all Joystick methods to read the most recent events from xboxdrv.
    The refreshRate determines the maximum events checked.
    If a valid event response is found, then the controller is 'connected'.
    """
    def refresh(self):
        """Refresh state."""
        # Refresh the joystick readings based on regular defined freq
        if self.refreshTime < time.time():
            # set next refresh time
            self.refreshTime = time.time() + self.refreshDelay
            # If there is text available to read from xboxdrv, then read it.
            readable, writeable, exception = select.select(
                [self.pipe], [], [], 0)
            if readable:
                # Read every line that is availabe.
                # We only need to decode the last one.
                while readable:
                    response = self.pipe.readline()
                    # A zero length response means
                    # controller has been unplugged.
                    if len(response) == 0:
                        raise IOError('Xbox controller disconnected from USB')
                    readable, writeable, exception = select.select(
                        [self.pipe], [], [], 0)
                # Valid controller response will be 140 chars.
                if len(response) == 140:
                    self.connectStatus = True
                    self.reading = response
                else:
                    self.connectStatus = False

    def connected(self):
        """Return connection status."""
        self.refresh()
        return self.connectStatus

    def leftX(self, deadzone=4000):
        """Return leftX status."""
        self.refresh()
        raw = int(self.reading[3:9])
        return self.axisScale(raw, deadzone)

    def leftY(self, deadzone=4000):
        """Return leftY status."""
        self.refresh()
        raw = int(self.reading[13:19])
        return self.axisScale(raw, deadzone)

    def rightX(self, deadzone=4000):
        """Return rightX status."""
        self.refresh()
        raw = int(self.reading[24:30])
        return self.axisScale(raw, deadzone)

    def rightY(self, deadzone=4000):
        """Return rightY status."""
        self.refresh()
        raw = int(self.reading[34:40])
        return self.axisScale(raw, deadzone)

    def axisScale(self, raw, deadzone):
        """Scaling axis from 0 to 1."""
        if abs(raw) < deadzone:
            return 0.0
        else:
            if raw < 0:
                return (raw + deadzone) / (32768.0 - deadzone)
            else:
                return (raw - deadzone) / (32767.0 - deadzone)

    def dpadUp(self):
        """Return dpadUp status."""
        self.refresh()
        return int(self.reading[45:46])

    def dpadDown(self):
        """Return dpadDown status."""
        self.refresh()
        return int(self.reading[50:51])

    def dpadLeft(self):
        """Return dpadLeft status."""
        self.refresh()
        return int(self.reading[55:56])

    def dpadRight(self):
        """Return dpadRight status."""
        self.refresh()
        return int(self.reading[60:61])

    def Back(self):
        """Return Back status."""
        self.refresh()
        return int(self.reading[68:69])

    def Guide(self):
        """Return Guide status."""
        self.refresh()
        return int(self.reading[76:77])

    def Start(self):
        """Return Start status."""
        self.refresh()
        return int(self.reading[84:85])

    def leftThumbstick(self):
        """Return leftThumbstick status."""
        self.refresh()
        return int(self.reading[90:91])

    def rightThumbstick(self):
        """Return rightThumbstick status."""
        self.refresh()
        return int(self.reading[95:96])

    def A(self):
        """Return A key status."""
        self.refresh()
        return int(self.reading[100:101])

    def B(self):
        """Return B key status."""
        self.refresh()
        return int(self.reading[104:105])

    def X(self):
        """Return X key status."""
        self.refresh()
        return int(self.reading[108:109])

    def Y(self):
        """Return Y key status."""
        self.refresh()
        return int(self.reading[112:113])

    def leftBumper(self):
        """Return leftBumper status."""
        self.refresh()
        return int(self.reading[118:119])

    def rightBumper(self):
        """Return rightBumper status."""
        self.refresh()
        return int(self.reading[123:124])

    def leftTrigger(self):
        """Return leftTrigger status."""
        self.refresh()
        return int(self.reading[129:132]) / 255.0

    def rightTrigger(self):
        """Return rightTrigger status."""
        self.refresh()
        return int(self.reading[136:139]) / 255.0

    def leftStick(self, deadzone=4000):
        """Return leftStick status."""
        self.refresh()
        return (self.leftX(deadzone), self.leftY(deadzone))

    def rightStick(self, deadzone=4000):
        """Return rightStick status."""
        self.refresh()
        return (self.rightX(deadzone), self.rightY(deadzone))

    # Cleanup by ending the xboxdrv subprocess
    def close(self):
        """Close process."""
        os.system('pkill xboxdrv')
